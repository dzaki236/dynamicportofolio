<?php
include "../koneksi_dan_proses/koneksi.php";
$id  = $_GET['id'];
$query_select = "SELECT * FROM blog WHERE id = '$id'";
$hasil = mysqli_query($connect, $query_select);
$postingan = mysqli_fetch_assoc($hasil);

// check apakah eror atau tidak?
if (mysqli_num_rows($hasil) < 1) {
    die('data gak di temukan');
}
?>

<!doctype html>
<?php include "../koneksi_dan_proses/koneksi.php";
if (isset($_POST['kirimpesan'])) {
    $nama = htmlspecialchars($_POST['nama']);
    $email = htmlspecialchars($_POST['email']);
    $pesan = htmlspecialchars($_POST['pesan']);
    $sequel = "INSERT INTO contact (nama,email,pesan,tanggal_contact)VALUES('$nama','$email','$pesan',CURRENT_TIMESTAMP)";

    $results = mysqli_query($connect, $sequel);
    if ($results) {
        echo "<script>alert('pesan telah dikirimkan!,tunggu balasan admin di email');
    document.location.href='index.php'
    </script>";
    } else {
        mysqli_errno($results);
    }
}
?>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS And A Loots-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="../owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="../owlcarousel/owl.theme.default.min.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.6/dist/sweetalert2.min.css">
    <title>Hello!</title>
    <style>
        /* Reset all margin and padding */
        body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        .heading_1 {
            font-size: 3em;
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: 700;
        }

        .heading_2 {
            font-size: 2.5em;
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: 700;
        }

        .big_heading {
            font-size: 4em;
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: 700;
        }

        .mt-6 {
            margin-top: 6em;
        }

        .navbar.navbar-white,
        .navbar button .navbar-toggler {
            border: none;
        }

        .card-img-top {
            width: 100%;
            height: 25vh;
            object-fit: contain;
        }
    </style>
</head>

<body>

    <div class="container mx-auto">
        <br>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a class="text-decoration-none" href="index.php">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Postingan</li>
            </ol>
        </nav>
        <br>
        <div class="card p-4">
        <h2 class="text-center text-dark"><?= $postingan['judul_blog']?></h2>    
        <?= $postingan['postingan_blog'] ?>
            <hr>
            <p>Author : <?= $postingan['author'] ?></p>Tanggal Posting : <?= date('d-F-Y', strtotime($postingan['tanggal_posting'])); ?><p></p>
        </div>
    </div>
    <div class="footer text-center p-3 bg-light">
        <p class="fw-bold text-dark">Copyright Dzaki Ahnaf Z @ 2021 || 11 RPL 2</p>
        <p>Made With <b>
                <3< /b>
        </p>
    </div>
    <!-- Option 1: Separate Popper and Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.6/dist/sweetalert2.all.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        function eror() {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Blog sedang masa pengembangan,tunggu dulu!',
                footer: 'Terima kasih :)'
            })
        }
    </script>
    <script>
        AOS.init();
    </script>
    <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="../owlcarousel/owl.carousel.min.js"></script>
    <!-- Call Owl plugins -->
    <script>
        $(document).ready(function() {
            $('.owl-carousel').owlCarousel({
                stagePadding: 50,
                center: true,
                loop: true,
                margin: 10,
                autoplay: true,
                lazyLoad: true,
                responsiveClass: true,
                autoHeight: true,
                autoplayTimeout: 7000,
                smartSpeed: 800,
                nav: true,
                navText: ['<span class="iconify mx-auto" style="width: 1em; height:1em" data-icon="ant-design:left-circle-filled" data-inline="false"></span>', '<span class="iconify" style="width: 1em; height:1em" data-icon="ant-design:right-circle-filled" data-inline="false"></span>'],
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 3,
                        nav: true
                    }
                }
            })
        });
    </script>
    <!-- Responsive jquery -->
    <script>
        $(document).ready(function() {
            $(window).scroll(function() {
                if ($(document).scrollTop() > 400) {

                    $("#nav_desktop").addClass('fixed-top shadow').css("transition", "0.3s");
                } else {
                    $("#nav_desktop").removeClass("fixed-top shadow").css("transition", "0.3s");
                }
            });
        });
    </script>
</body>

</html>