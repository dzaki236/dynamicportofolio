    <!doctype html>
    <?php include "../koneksi_dan_proses/koneksi.php";
    if (isset($_POST['kirimpesan'])) {
        $nama = htmlspecialchars($_POST['nama']);
        $email = htmlspecialchars($_POST['email']);
        $pesan = htmlspecialchars($_POST['pesan']);
        $sequel = "INSERT INTO contact (nama,email,pesan,tanggal_contact)VALUES('$nama','$email','$pesan',CURRENT_TIMESTAMP)";

        $results = mysqli_query($connect, $sequel);
        if ($results) {
            echo "<script>alert('pesan telah dikirimkan!,tunggu balasan admin di email');
    document.location.href='index.php'
    </script>";
        } else {
            mysqli_errno($results);
        }
    }
    ?>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS And A Loots-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
        <link rel="stylesheet" href="../owlcarousel/owl.carousel.min.css">
        <link rel="stylesheet" href="../owlcarousel/owl.theme.default.min.css">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.6/dist/sweetalert2.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap" rel="stylesheet">

        <title>Hello!</title>
        <style>
            /* Reset all margin and padding */
            body {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                scroll-behavior: smooth;
                background-color:#fafdff;
            }

            .heading_1 {
                font-size: 3em;
                font-family: 'Source Sans Pro', sans-serif;
                font-weight: 700;
            }

            .heading_2 {
                font-size: 2.5em;
                font-family: 'Source Sans Pro', sans-serif;
                font-weight: 700;
            }

            .big_heading {
                font-size: 4em;
                font-family: 'Source Sans Pro', sans-serif;
                font-weight: 700;
            }

            .mt-6 {
                margin-top: 6em;
            }

            .navbar.navbar-white,
            .navbar button .navbar-toggler {
                border: none;
            }

            .card-img-top {
                width: 100%;
                height: 25vh;
                object-fit: contain;
            }
            .birutua{
                color : #009dff !important;
            }
            .birumuda{
                color: #668cff !important;
            }
            .birugelap{
                color: #101930 !important;
            }
            hr{
                background-color: #192030;
                width: 90%;
                margin:auto;
            }
        </style>
    </head>

    <body>
        <div class="container-fluid">

            <!-- Header start -->
            <div class="row">
                <div class="col-md-4 profil"><img class="img-fluid" src="../image/poto.jpg" data-aos="zoom-in-right" data-aos-duration="3000" alt=""></div>
                <div class="col-md-8 mt-2">
                    <!-- Navbar desktop version -->
                    <div id="nav_desktop">
                        <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-white">
                            <div class="container-fluid">
                                <a class="navbar-brand birumuda" href="#">Dzaki Ahnaf Z</a>
                                <button class="navbar-toggler border border-white" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                                    <ul class="navbar-nav">
                                        <li class="nav-item ms-4">
                                            <a class="nav-link" aria-current="page" href="#home">Home</a>
                                        </li>
                                        <li class="nav-item ms-4">
                                            <a class="nav-link" href="#projects">Projects</a>
                                        </li>
                                        <li class="nav-item ms-4">
                                            <a class="nav-link" href="#blog">Blog</a>
                                        </li>
                                        <li class="nav-item ms-4">
                                            <a class="nav-link" href="#contacts">Contact Me</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <!-- End navbar mobile desktop version -->
                    <br>
                    <hr>
                    <br>
                    <!-- heading start -->
                    <div id="home" class="text-heading text-center mx-auto" data-aos="fade-up" data-aos-duration="1000">
                        <h1 class="heading_1">HELLO I'M</h1>
                        <h2 class="big_heading  text-primary">Dzaki Ahnaf Zulfikar</h2>
                        <p class="h5 text-secondary"><q>Student On Smk Taruna Bhakti</q></p>
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-4 mt-2 mb-4"><a class="text-dark" href="https://www.instagram.com/lemonic.dev_/"><i class="bi h2 bi-instagram me-5"></i></a> <a class="text-dark" href="http://github.com/dzaki236/"><i class="bi h2 bi-github me-5"></i></a><a class="text-dark" href="#"><i class="h2 bi bi-telegram"></i></a></div>

                        </div>
                    </div>

                    <!-- heading end -->
                </div>
            </div>
            <!-- Header end -->
            <hr>
            <br>
            <!-- Introduction & School History -->
            <div class="row bg-white shadow m-4 pt-5 pb-5 border border-light border-2 rounded-3" data-aos="fade-up" data-aos-duration="2000">
                <div class="row mx-auto">
                    <div class="col-lg-4 mb-3">
                        <div class="list-group" id="list-tab" role="tablist">
                            <a class="list-group-item bg-white text-dark p-4 list-group-item-action active" id="list-home-list" data-bs-toggle="list" href="#list-home" role="tab" aria-controls="home">Perkenalan</a>
                            <hr class="mt-4 mb-4">
                            <a class="list-group-item bg-white text-dark p-4 list-group-item-action" id="list-pendidikan-list" data-bs-toggle="list" href="#list-pendidikan" role="tab" aria-controls="pendidikan">Riwayat Pendidikan</a>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                                <h2 class="text-center">Perkenalan</h2>
                                <div class="text-secondary">
                                    <?php $sql_tentang = "SELECT * FROM tentang_saya";
                                    $tentang = mysqli_query($connect, $sql_tentang);
                                    foreach ($tentang as $about) :
                                    ?>
                                        <?= $about['isi_tentang_saya']; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="list-pendidikan" role="tabpanel" aria-labelledby="list-pendidikan-list">
                                <h2 class="text-center">History Pendidikan</h2>
                                <ul>
                                    <?php $sql_pendidikan = "SELECT * FROM pendidikan_status";
                                    $pendidikan = mysqli_query($connect, $sql_pendidikan);
                                    foreach ($pendidikan as $pendidikan_status) :
                                    ?>
                                        <li>
                                            <h6 class="fw-bold text-secondary"><?= $pendidikan_status['tahun_masuk'] . "-" . $pendidikan_status['tahun_lulus'] ?> (<?= $pendidikan_status['tingkat_pendidikan'] ?>)</h6>
                                            <p class="birugelap" style="letter-spacing:0.08em;"><?= $pendidikan_status['nama_sekolah'] ?></p>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of both -->
            <br>
            <hr>
            <br>
            <!-- My Skills -->
            <div id="projects" class="row mt-5 mb-5" data-aos="fade-up" data-aos-duration="2000">
                <br>
                <h1 class="fw-bold text-center">My Skills</h1>
                <p class="text-muted text-center fst-italic font-monospace">Berikut beberapa skill yang telah saya pelajari</p>
                <br>
                <div class="row mt-5 mx-auto">
                    <div class="col-md-4 col-lg-4 mb-4 text-center">
                        <div class="card bg-transparent" style="width: 18; border:none;">
                            <i class="iconify mx-auto" style="width: 8em; height:8em" data-icon="bi:laptop" data-inline="false"></i>
                            <div class="card-body">
                                <h5 class="card-title fw-bolder">Back-End & Front-End</h5>
                                <p class="card-text text-secondary">Laravel | MySQL | Bootstrap | Node.js | Vue.js</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 mb-4 text-center">
                        <div class="card bg-transparent" style="width: 18; border:none;">
                            <i class="iconify mx-auto" style="width: 8em; height:8em" data-icon="bx:bxs-layer" data-inline="false"></i>
                            <div class="card-body">
                                <h5 class="card-title fw-bolder">UI/UX Designs</h5>
                                <p class="card-text text-secondary">Adobe XD | Figma | Artboard Studio</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 mb-4 text-center">
                        <div class="card bg-transparent" style="width: 18; border:none;">
                            <i class="iconify mx-auto" style="width: 8em; height:8em" data-icon="ph:circles-three-bold" data-inline="false"></i>
                            <div class="card-body">
                                <h5 class="card-title fw-bolder">Others</h5>
                                <p class="card-text text-secondary">After Effect | Alight Motion | Premiere Pro</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of both -->
            <br>
            <hr>
            <br>
            <!-- My Project -->
            <div class="row mt-5 mb-5" data-aos="fade-up" data-aos-duration="2000">
                <br>
                <h1 class="fw-bold text-center">My Project</h1>
                <p class="text-muted text-center fst-italic font-monospace">Berikut beberapa project yang telah saya kerjakan</p>
                <br>
                <div class="row mt-5 mx-auto">
                    <?php $sql_project = "SELECT * FROM projects";
                    $projek = mysqli_query($connect, $sql_project);
                    foreach ($projek as $projects) :
                    ?>
                        <div class="col-md-4 col-lg-4 mb-4 text-center">
                            <div class="card bg-transparent" style="width: 18; border:none;">
                                <img src="<?= $projects['foto_project'] ?>" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title fw-bold"><?= $projects['nama_project'] ?></h5>
                                    <p class="card-text"><?= $projects['penjelasan_singkat_project'] ?></p>
                                    <a href="<?= $projects['link_project'] ?>" class="btn text-dark">See Project > </a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
            <!-- End of both -->
            <hr id="blog">
            <!-- My Blog -->
            <div class="row mt-5 mb-5" data-aos="fade-up" data-aos-duration="2000">
                <h1 class="text-center fw-bold">Blog</h1>
                <p class="text-muted text-center fst-italic font-monospace">Referensi tulisan apa yang telah di pelajari</p>
                <!-- Set up your HTML -->

                <div class="owl-carousel mt-5 owl-theme">
                    <?php $sql_postingan = "SELECT * FROM blog";
                    $posts = mysqli_query($connect, $sql_postingan);
                    foreach ($posts as $postingan) :
                    ?>
                        <div class="item">
                            <div class="card" style="width: 18rem;">
                                <img src="<?= $postingan['thumbnail_blog'] ?>" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title"><?= $postingan['judul_blog'] ?></h5>
                                    <p class="card-text">Author : <?= $postingan['author'] ?></p>
                                    <a class="btn" href="./blog.php?id=<?= $postingan['id']; ?>">Read Blog</a>
                                </div>
                            </div>
                        </div> <?php endforeach; ?>
                </div>

            </div>
            <!-- End of both -->
            <hr id="contacts">
            <!-- Contact me -->
            <div class="row mt-5 mb-5" data-aos="fade-up" data-aos-duration="2000">
                <h1 class="text-center fw-bold">Tertarik Bekerja sama dengan saya?</h1>
                <p class="text-muted fs-5 text-center fst-italic font-monospace">Contact saya disini</p>
                <div class="bg-white ps-5 pe-5">
                    <form action=" " method="POST">
                        <div class="mb-3">
                            <label for="exampleInputText1" class="form-label">Nama</label>
                            <input type="text" class="form-control" id="exampleInputText1" aria-describedby="emailHelp" name="nama" required>
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Alamat Email</label>
                            <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                            <div id="emailHelp" class="form-text">Email tak akan di beritahu kesiapapun</div>
                        </div>
                        <div class=" mb-3 form-floating">
                            <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px" name="pesan" required></textarea>
                            <label for="floatingTextarea2">Sampaikan Pesanmu Disini</label>
                        </div>
                        <div id="emailHelp" class="form-text">Harap berhati hati dalam mengirim pesan,karena pesan tak dapat di urungkan kembali.</div>
                        <div class="d-grid gap-3 mt-1">
                            <button type="submit" name="kirimpesan" value="kirim" class="btn btn-primary">Kirim Pesan</button>

                        </div>
                    </form>
                </div>
            </div>
            <!-- End of both -->

        </div>
        <div class="footer text-center p-3 bg-light">
            <p class="fw-bold text-dark">Copyright Dzaki Ahnaf Z @ 2021 || 11 RPL 2</p>
            <p>Made With <b>
                    luv </b>
            </p>
        </div>
        <!-- Option 1: Separate Popper and Bootstrap JS -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.6/dist/sweetalert2.all.min.js"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script>
            function eror() {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Blog sedang masa pengembangan,tunggu dulu!',
                    footer: 'Terima kasih :)'
                })
            }
        </script>
        <script>
            AOS.init();
        </script>
        <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="../owlcarousel/owl.carousel.min.js"></script>
        <!-- Call Owl plugins -->
        <script>
            $(document).ready(function() {
                $('.owl-carousel').owlCarousel({
                    stagePadding: 50,
                    center: true,
                    loop: true,
                    margin: 10,
                    autoplay: true,
                    lazyLoad: true,
                    responsiveClass: true,
                    autoHeight: true,
                    autoplayTimeout: 7000,
                    smartSpeed: 800,
                    nav: true,
                    navText: ['<span class="iconify mx-auto" style="width: 1em; height:1em" data-icon="ant-design:left-circle-filled" data-inline="false"></span>', '<span class="iconify" style="width: 1em; height:1em" data-icon="ant-design:right-circle-filled" data-inline="false"></span>'],
                    responsive: {
                        0: {
                            items: 1,
                            nav: true
                        },
                        600: {
                            items: 3,
                            nav: true
                        }
                    }
                })
            });
        </script>
        <!-- Responsive jquery -->
        <script>
            $(document).ready(function() {
                $(window).scroll(function() {
                    if ($(document).scrollTop() > 400) {

                        $("#nav_desktop").addClass('fixed-top shadow').css("transition", "0.3s");
                    } else {
                        $("#nav_desktop").removeClass("fixed-top shadow").css("transition", "0.3s");
                    }
                });
            });
        </script>
    </body>

    </html>