<?php 
session_start();   
 
if(isset($_SESSION['username'])){       
$username = $_SESSION['username'];
}
else{header("location:login.php");
}   

require "../koneksi_dan_proses/koneksi.php";

$tampil_post = mysqli_query($connect,"SELECT * FROM blog");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Dashboard - DZ Admin</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <!-- Navbar -->
    <?php include "./components/navbar.php"; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <!-- Sidebar -->
            <?php include "./components/sidebar.php"; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="card-body">
                    <h1 class="text-center">Blog</h1>
                    <a href="./tambahDataPostingan.php" class="btn btn-outline-secondary mb-3">[+] Tambah Postingan</a>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Author</th>
                                    <th>Thumbnail blog</th>
                                    <th>Judul postingan</th>
                                    <th>Postingan</th>
                                    <th>Tanggal Posting</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Author</th>
                                    <th>Thumbnail blog</th>
                                    <th>Judul postingan</th>
                                    <th>Postingan</th>
                                    <th>Tanggal Posting</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php $no = 1; foreach($tampil_post as $postingan):?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $postingan['author']?></td>
                                    <td><img src="<?= $postingan['thumbnail_blog']?>" width="50%" alt=""></td>
                                    <td><?= $postingan['judul_blog']?></td>
                                    <td><a href="./detailDataPostingan.php?id=<?=$postingan['id'];?>">Lihat postingan </a></td>
                                    <td width="150px"><?= date('d-F-Y',strtotime($postingan['tanggal_posting']))?></td>
                                    <td><a href="./editDataPostingan.php?id=<?=$postingan['id'];?>" class="btn btn-outline-warning btn-block"><i class="far fa-edit"></i></a><a href="../koneksi_dan_proses/hapusPostingan.php?id=<?=$postingan['id'];?>" class="btn btn-danger btn-block mt-3"><i class="fas fa-trash-alt"></i></a><sub>data yang di hapus tak bisa di kembalikan</sub></td>   
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <!-- Footer -->
                <?php include "./components/footer.php" ?>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
</body>

</html>