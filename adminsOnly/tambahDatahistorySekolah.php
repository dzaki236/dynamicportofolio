<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Dashboard - DZ Admin</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <!-- Navbar -->
    <?php include "./components/navbar.php"; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <!-- Sidebar -->
            <?php include "./components/sidebar.php"; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-sm mt-4">
                    <form action="../koneksi_dan_proses/addhistorySekolah.php" method="POST">
                        <div class="form-group">
                            <h1 class="text-center">Tambah Riwayat</h1>
                            <label>Tingkat Pendidikan</label>
                            <select name="tingkat_pendidikan" class="form-control" id="">
                            <option value="Belum Memilih">---Belum Memilih---</option>
                                <option value="Sarjana1">S1</option>
                                <option value="SMA/SMK">SMA/SMK</option>
                                <option value="SMP">SMP</option>
                                <option value="SD">SD</option>
                            </select>
                            <small class="form-text text-muted">pilih kecuali opsi "belum memilih"</small>
                        </div>
                        <br>
                        <div class="form-group">
                            <label>Nama Sekolah / Diploma</label>
                            <input type="text" class="form-control" name="nama_sekolah" placeholder="Misal Smk/smp/sd n 5 depok">
                        </div>
                        <br>
                        <div class="form-group">
                            <label>Tahun Masuk</label>
                            <input type="number" class="form-control" name="tahun_masuk" placeholder="Misal 2010">
                        </div>
                        <br>
                        <div class="form-group">
                            <label>Tahun Lulus</label>
                            <input type="number" class="form-control" name="tahun_lulus" placeholder="Misal 2016">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block" value="tambahSekolah" name="tambah_riwayat">Tambah Riwayat</button>
                        </div>
                    </form>
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <!-- Footer -->
                <?php include "./components/footer.php" ?>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/jquery.tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        $('textarea#tiny').tinymce({
            height: 500,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
        });
    </script>
</body>

</html>