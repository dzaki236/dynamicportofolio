<?php 
session_start();   
 
if(isset($_SESSION['username'])){       
$username = $_SESSION['username'];}
else{header("location:login.php");}   
require "../koneksi_dan_proses/koneksi.php";
$pendidikan = mysqli_query($connect,"SELECT * FROM pendidikan_status");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Dashboard - DZ Admin</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <!-- Navbar -->
    <?php include "./components/navbar.php"; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <!-- Sidebar -->
            <?php include "./components/sidebar.php"; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="card-body">
                    <h1 class="text-center">Riwayat Pendidikan formal</h1>
                    <a href="./tambahDatahistorySekolah.php" class="btn btn-outline-secondary mb-3">[+] Tambah Riwayat Pendidikan </a>
                    <?php foreach($pendidikan as $history_pendidikan):?>
                    <div class="m-5 p-5">
                       <span> - <h4 class="text-secondary"><?= $history_pendidikan['tingkat_pendidikan']?></h4></span>
                    <p><?= $history_pendidikan['nama_sekolah']?></p>
                    <p>tahun masuk : <?= $history_pendidikan['tahun_masuk']?></p>
                    <p>tahun lulus : <?= $history_pendidikan['tahun_lulus']?></p>
                    <a href="editDatahistorySekolah.php?id=<?=$history_pendidikan['id'];?>" class="btn"><i class="far fa-edit"></i></a>
                    <a href="../koneksi_dan_proses/hapushistorySekolah.php?id=<?=$history_pendidikan['id'];?>" class="btn text-danger"><i class="fas fa-trash"></i></a>
                    </div>
                    <?php endforeach;?>
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <!-- Footer -->
                <?php include "./components/footer.php" ?>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
</body>

</html>