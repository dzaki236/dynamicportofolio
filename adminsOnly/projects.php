<?php
session_start();   
 
if(isset($_SESSION['username'])){       
$username = $_SESSION['username'];}
else{header("location:login.php");}   

require "../koneksi_dan_proses/koneksi.php";
$tampil_project = mysqli_query($connect, "SELECT * FROM projects");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Dashboard - DZ Admin</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <!-- Navbar -->
    <?php include "./components/navbar.php"; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <!-- Sidebar -->
            <?php include "./components/sidebar.php"; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="card-body">
                    <h1 class="text-center">Projects</h1>
                    <a href="./tambahDataProject.php" class="btn btn-outline-secondary mb-3">[+] Tambah Project</a>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Nama Projek</th>
                                    <th>Foto Project</th>
                                    <th>Penjelasan Singkat Project</th>
                                    <th>Link Project</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Nama Projek</th>
                                    <th>Foto Project</th>
                                    <th>Penjelasan Singkat Project</th>
                                    <th>Link Project</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php $no = 1;
                                foreach ($tampil_project as $projects) : ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $projects['nama_project'] ?></td>
                                        <td><img src="<?= $projects['foto_project'] ?>" width="50%" alt=""></td>
                                        <td><?= $projects['penjelasan_singkat_project'] ?></td>
                                        <td><a href="<?= $projects['link_project']; ?>">Link Project</a></td>
                                        
                                        <td><a href="./editDataProjects.php?id=<?= $projects['id']; ?>" class="btn btn-outline-warning btn-block"><i class="far fa-edit"></i></a><a href="../koneksi_dan_proses/hapusProjects.php?id=<?= $projects['id']; ?>" class="btn btn-danger btn-block mt-3"><i class="fas fa-trash-alt"></i></a><sub>data yang di hapus tak bisa di kembalikan</sub></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <!-- Footer -->
                <?php include "./components/footer.php" ?>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
</body>

</html>