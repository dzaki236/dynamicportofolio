<?php          
session_start();   
 
if(isset($_SESSION['username'])){       
$username = $_SESSION['username'];}
else{header("location:login.php");} 

include "../koneksi_dan_proses/koneksi.php";
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - DZ Admin</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <!-- Navbar -->
        <?php include "./components/navbar.php";?>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <!-- Sidebar -->
                <?php include "./components/sidebar.php";?>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <br>
                    <h1 class="fs-6 mt-4 text-center"> SELAMAT DATANG ADMIN!</h1>
                    <br>
                    <div class="row m-4">
                            <div class="col-xl-4 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body">
                                        <h1><?php 
                                        $query_ = "SELECT COUNT(*) AS TotalBlog FROM blog";
                                        $result = mysqli_query($connect,$query_);
                                        $tampil_post=mysqli_fetch_assoc($result);
                                        echo($tampil_post['TotalBlog']);
                                        ?>
                                        </h1>
                                        Postingan Blog</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="./postinganBlog.php">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="card bg-success text-white mb-4">
                                
                                    <div class="card-body"><h1><?php 
                                        $query_2 = "SELECT COUNT(*) AS TotalProject FROM projects";
                                        $result = mysqli_query($connect,$query_2);
                                        $tampil_projects=mysqli_fetch_assoc($result);
                                        echo($tampil_projects['TotalProject']);
                                        ?>
                                        </h1>Project</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="./projects.php">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="card bg-light text-primary mb-4">
                                    <div class="card-body"><h1><?php 
                                        $query_2 = "SELECT COUNT(*) AS TotalContact FROM contact";
                                        $result = mysqli_query($connect,$query_2);
                                        $tampil_contacts=mysqli_fetch_assoc($result);
                                        echo($tampil_contacts['TotalContact']);
                                        ?>
                                        </h1>Contact masuk</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-primary stretched-link" href="./contacts.php">View Details</a>
                                        <div class="small text-primary"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <!-- footer -->
                    <?php include "./components/footer.php";?>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/datatables-demo.js"></script>
    </body>
</html>
