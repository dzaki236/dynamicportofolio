<?php 
session_start();   
// login sesstion
if(isset($_SESSION['username'])){       
$username = $_SESSION['username'];}
else{header("location:login.php");}   
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Dashboard - DZ Admin</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <!-- Navbar -->
    <?php include "./components/navbar.php"; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <!-- Sidebar -->
            <?php include "./components/sidebar.php"; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-sm mt-4">
                <form action="../koneksi_dan_proses/addPostingan.php" method="POST">
                    <div class="form-group">
                        <label>author/pembuat</label>
                        <input type="hidden" class="form-control" placeholder="author" name="author" value="<?=$_SESSION['username'];?>">
                        <p><?=$_SESSION['username'];?></p>
                        <small class="form-text text-muted">default : admin</small>
                    </div>
                    <div class="form-group">
                        <label>Thumbnail Postingan</label>
                        <input type="text" class="form-control" name="thumbnail_blog" placeholder="https://www.img.com/img.png">
                        <small class="form-text text-muted">masukkan link gambar dari internet</small>
                    </div>
                    <div class="form-group">
                        <label>Judul Postingan</label>
                        <input type="text" class="form-control" name="judul_blog" placeholder="judul untuk postingan ini">
                    </div>
                    <div class="form-group">
                        <label>Postingan Blog</label>
                        <div>
                            <textarea id="tiny" name="postingan_blog"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block" value="tambahPostingan" name="tambah_post">Tambah Postingan</button>
                    </div>
                </form></div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <!-- Footer -->
                <?php include "./components/footer.php" ?>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/jquery.tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        $('textarea#tiny').tinymce({
            height: 500,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
        });
    </script>
</body>

</html>