<?php
session_start();   
 
if(isset($_SESSION['username'])){       
$username = $_SESSION['username'];}
else{header("location:login.php");}   
include "../koneksi_dan_proses/koneksi.php";
$id = $_GET['id'];

if(!isset($id)){
    header('Location: ./postinganBlog.php');
}
$selectquery = "SELECT * FROM projects WHERE id = '$id'";
echo $selectquery;
$query = mysqli_query($connect,$selectquery);
$projects = mysqli_fetch_assoc($query);

// check apakah eror atau tidak?
if(mysqli_num_rows($query)<1){
    die('data gak di temukan');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Dashboard - DZ Admin</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <!-- Navbar -->
    <?php include "./components/navbar.php"; ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <!-- Sidebar -->
            <?php include "./components/sidebar.php"; ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-sm mt-4">
                <form action="../koneksi_dan_proses/editProjects.php?id=<?= $id;?>" method="POST">
                <br>
                        <div class="form-group">
                            <label>Nama Project</label>
                            <input type="text" class="form-control" value="<?= $projects['nama_project'];?>" name="nama_project" required placeholder="misal: project iseng">
                        </div>
                        <br>
                        <div class="form-group">
                            <label>Link Foto Project</label>
                            <input type="text" class="form-control" value="<?= $projects['foto_project'];?>" name="foto_project" required placeholder="misal: www.heroku.com/index/photo.jpg">
                        </div>
                        <br>
                        <div class="form-group">
                            <label>Penjelasan Singkat</label>
                            <div>
                                <textarea id="tiny" name="penjelasan_singkat_project" placeholder="misal : project untuk membantu masyarakat"><?= $projects['penjelasan_singkat_project'];?></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label>Link Project</label>
                            <input type="text" class="form-control" name="link_project" placeholder="misal: www.heroku.com/index" required value="<?= $projects['link_project'];?>">
                        </div>
                        <br>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block" value="edit" name="edit_project">Edit Project</button>
                        </div>

                </form></div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <!-- Footer -->
                <?php include "./components/footer.php" ?>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/jquery.tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        $('textarea#tiny').tinymce({
            height: 500,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
        });
    </script>
</body>

</html>